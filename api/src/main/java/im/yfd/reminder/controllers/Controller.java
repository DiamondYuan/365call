package im.yfd.reminder.controllers;


import im.yfd.reminder.domain.ResultWrapper;
import im.yfd.reminder.domain.dto.ChangeAccountRequestDto;
import im.yfd.reminder.domain.dto.UserInfoDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@CrossOrigin
@Slf4j
public class Controller {

  @GetMapping(path = "api/v1/account")
  public ResultWrapper<UserInfoDTO> getUserInfo() {
    UserInfoDTO temp = new UserInfoDTO() {{
      setClockTime(new Date());
      setMobile("1899999999");

    }};
    return new ResultWrapper<>(temp);
  }

  @PostMapping(path = "api/v1/account")
  public ResultWrapper<String> createAccount() {
    return new ResultWrapper<>("success");
  }

  @PutMapping(path = "api/v1/account")
  public ResultWrapper<String> createAccount(
    @RequestBody ChangeAccountRequestDto changeAccountRequestDto
  ) {
    System.out.println(changeAccountRequestDto);
    return new ResultWrapper<>("success");
  }

  @GetMapping(path = "api/v1/clock")
  public ResultWrapper<String> clock() {
    return new ResultWrapper<>("success");
  }
}
