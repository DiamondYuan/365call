package im.yfd.reminder;

import com.google.common.collect.ImmutableMap;
import im.yfd.reminder.domain.GenericException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Map;


@Slf4j
@ControllerAdvice(basePackages = "im.yfd.reminder.controllers")
public class GlobalControllerExceptionHandler {

  @ResponseBody
  @ResponseStatus(HttpStatus.OK)
  @ExceptionHandler({GenericException.class})
  public Map<String, String> handleException(GenericException e) {
    return ImmutableMap.of("code", e.getCode(), "message", e.getMessage());
  }

}
