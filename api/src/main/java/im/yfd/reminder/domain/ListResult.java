package im.yfd.reminder.domain;

import lombok.Data;

import java.util.List;


@Data
public class ListResult<T> {
  private List<T> list;
  private int size;

  public ListResult(List<T> list) {
    this.list = list;
    if (list == null) {
      this.size = 0;
    } else {
      this.size = list.size();
    }
  }
}
