package im.yfd.reminder.domain.dto;

import lombok.Data;

@Data
public class ChangeAccountRequestDto {
  private String emergencyContact;
}
