package im.yfd.reminder.domain.dto;

import lombok.Data;

import java.util.Date;

@Data
public class UserInfoDTO {
  private String mobile;
  private Date clockTime;
  private String emergencyContact;
}
