package im.yfd.reminder.domain.dto;

import lombok.Data;

@Data
public class CreateAccountRequest {
  private String mobile;
  private String password;
}
