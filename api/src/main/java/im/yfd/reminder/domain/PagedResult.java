package im.yfd.reminder.domain;

import lombok.Data;

import java.util.List;

/**
 * @author Administrator
 * @date 2017/3/10
 */
@Data
public class PagedResult<T> {
  private List<T> list;
  private Integer size;
  private Integer pageSize;
  private Integer page;
}
