package im.yfd.reminder.domain;

import lombok.Data;


@Data
public class ResultWrapper<T> {
  private int code;
  private String message;
  private T data;

  @SuppressWarnings("unchecked")
  public ResultWrapper(T data) {
    this.code = 1000000;
    this.message = "success";
    this.data = data;
  }


  public ResultWrapper(int code, String message, T data) {
    this.code = code;
    this.message = message;
    this.data = data;
  }

  public static <T> ResultWrapper<T> of(T data) {
    return new ResultWrapper<>(1000000, "ok", data);
  }

  @SuppressWarnings("SameParameterValue")
  public static <T> ResultWrapper<T> of(int code, String message, T data) {
    return new ResultWrapper<>(code, message, data);
  }


}
