package im.yfd.reminder.domain;

import lombok.Data;

import java.util.Date;

@Data
public class UserPO {
  private Integer id;
  private String mobile;
  private String password;
  private Date createdAt;
  private Date modifiedAt;
  private Date clockTime;
  private Date remindAt;
  private String emergencyContact;
}
