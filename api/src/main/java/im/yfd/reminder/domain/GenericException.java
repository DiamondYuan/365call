package im.yfd.reminder.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @author Diamond
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class GenericException extends Exception {
  private String code;

  public GenericException(String code, String message) {
    super(message);
    this.setCode(code);
  }

  @Data
  public static class GenericExceptionApiDoc {
    private String message;
    private String code;
  }
}
